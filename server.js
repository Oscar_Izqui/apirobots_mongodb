const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//inicialitzem mongoose
const db = require("mongoose");
db.Promise = global.Promise;

const conn_url = "mongodb://localhost:27017/robots_db";

//connectem a la bdd
db.connect(conn_url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("Connexió OK!");
    })
    .catch(err => {
        console.log("Connexió falla...", err);
        process.exit();
    });

db.set('useFindAndModify', false);

//creem esquema i model 'Robots' && 'Batallas'
const robotsCollection = db.Schema(
    {
        nombre: String,
        caracteristicas: {
            ataque: Number,
            defensa: Number,
            vida: Number
        },
        experiencia: {
            victorias: Number,
            jugadas: Number
        }
    }
);
const Robot = db.model('robot', robotsCollection);

const batallasCollection = db.Schema({ id_ganador: String, id_perdedor: String });
const Batalla = db.model('batalla', batallasCollection);

// definim rutes de la API
app.get("/", (req, res) => {
    res.send("<h1>Benvingut a la API de Robots asesinos!</h1>");
});

// consulta TOTS els robots
app.get("/api/robots", (req, res) => {
    Robot.find()
        .then(data => {
            // hacemos cosas con los datos antes de enviar...
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// consulta UN robot per ID
app.get("/api/robots/:id", (req, res) => {
    const id = req.params.id;
    Robot.findById(id)
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// crea un robot
app.post("/api/robots", (req, res) => {
    if (!req.body.nombre) {
        res.status(400).send({ error: "No trobo el robot!" });
        return;
    }

    // robot nou
    const robot = new Robot({
        nombre: req.body.nombre,
        caracteristicas: {
            ataque: req.body.ataque,
            defensa: req.body.defensa,
            vida: req.body.vida
        },
        experiencia: {
            victorias: 0,
            jugadas: 0
        }
    });

    // el guardem a la bdd

    robot.save(robot)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

//elimina un robot
app.delete("/api/robots/:id", (req, res) => {
    const id = req.params.id;
    Robot.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(500).send({ error: "No s'ha eliminat res" });
            } else {
                res.send({ msg: "Eliminat", id: id });
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// modifica un robot
app.put("/api/robots/:id", (req, res) => {
    if (!req.body) {
        res.status(400).send({ error: "No trobo dades!" });
        return;
    }

    const id = req.params.id;

    Robot.findByIdAndUpdate(id, req.body)
        .then(data => {
            if (!data) {
                res.send({ error: "No s'ha actualitzat res" });
            } else {
                res.send({ msg: "Actualitzat" });
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// BATALLA!
app.get("/api/batalla/:id1/:id2", async (req, res) => {
    const id1 = req.params.id1;
    const id2 = req.params.id2;
    const robot1 = await Robot.findById(id1);
    const robot2 = await Robot.findById(id2);

    const combate = () => {
        let ret = false;
        
        const vida1 = robot1.caracteristicas.vida - (robot2.caracteristicas.ataque - robot1.caracteristicas.defensa);
        const vida2 = robot2.caracteristicas.vida - (robot1.caracteristicas.ataque - robot2.caracteristicas.defensa);

        if (vida1 > vida2){
            ret = true;
        } else {
            ret = false;
        }

        return ret;
    }

    const ganador = combate() ? robot1 : robot2;
    res.status(200).json(ganador);

});

// consulta TOTES les batalles
app.get("/api/partidas", (req, res) => {
    Batalla.find()
        .then(data => {
            // hacemos cosas con los datos antes de enviar...
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

app.post("/api/partidas", (req, res) => {
    if (!req.body.id_ganador) {
        res.status(400).send({ error: "No trobo la partida!" });
        return;
    }

    // partida nova
    const batalla = new Batalla({
        id_ganador: req.body.id_ganador,
        id_perdedor: req.body.id_perdedor
    });

    // el guardem a la bdd

    batalla.save(batalla)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// consulta robots por ESTADISTICAS
app.get("/api/estadisticas", (req, res) => {
    Robot.find()
        .then(data => {
            // hacemos cosas con los datos antes de enviar...
            const temp = [...data];
            temp.sort((a, b) => (a[a.experiencia.victorias] > b[b.experiencia.victorias]) ? 1 : -1);
            res.status(200).json(temp);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// set port, listen for requests
const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});